# Raspberry Pi Code

**websocket-sever-pi.py:** first implementation (act as server) of websockets on the Pi
<br>
**websocket-client-pi.py:** final implementation (act as client) of websockets on the Pi
<br>
**servo-testing.py:** test and set up for servos
<br>
