from flask import Flask
from flask_cors import CORS # sec by pass
from flask_socketio import SocketIO
import time

from gpiozero import Servo
from gpiozero.pins.pigpio import PiGPIOFactory
import math

import base64
import io
from picamera import PiCamera


factory = PiGPIOFactory()
servo1 = Servo(23, min_pulse_width=0.5/1000, max_pulse_width=2.5/1000, pin_factory=factory)

app = Flask(__name__)

CORS(app, resources={r"/socket.io/*": {"origins": "*"}})
 
# socket
# socketio = SocketIO(app, cors_allowed_origins="http://localhost:8000", logger=True)

# standard
socketio = SocketIO(app)


@app.route('/') 
def index():
    counter = 2 
    while counter >= 0:
        print('Ping',  counter)
        time.sleep(0.05)
        counter = counter - 1
    return 'Inside index'

@app.route('/move_servo/<angle>')
def move_servo(angle):
    print(f'The angle is {angle}')
    time.sleep(0.01)
    servo1.value = math.sin(math.radians(int(angle)))
    return f'The angle is {angle}'
    
@app.route('/center_servo')
def center_servo():
    time.sleep(0.01)
    servo1.mid()
    return 'Servo centered'

@app.route('/capture_image')
def capture_image():
    camera = PiCamera(resolution=(640,480))
    camera.brightness=60 # above 50 = increase
    camera.contrast = 30 # positive = increase
    camera.vflip=True
    image_stream = io.BytesIO()
    
    camera.start_preview()
    camera.capture(image_stream, format='jpeg')
    
    time.sleep(0.1)
    camera.stop_preview()
    camera.close()
     
    image_stream.seek(0)
    
    image_data = base64.b64encode(image_stream.read()).decode('utf-8')
    
    print('Got image data')
    
    return f'{image_data}'

@socketio.on('connect')
def hangle_connect():
    print('Client connected')

@socketio.on('get_image')
def get_image():
    camera = PiCamera(resolution=(640,480))
    camera.brightness=60 # above 50 = increase
    camera.contrast = 30 # positive = increase
    camera.vflip=True

    image_stream = io.BytesIO()
    
    camera.start_preview()
    time.sleep(0.01)
    camera.capture(image_stream, format='jpeg')
    camera.stop_preview()
    camera.close()
    
    image_stream.seek(0)
    
    image_data = base64.b64encode(image_stream.read()).decode('utf-8')
    
    print('Got image data')
    
    return f'{image_data}'

if __name__=='__main__':
    # web socket
    #socketio.run(app, host='10.0.0.140', port=5000, debug=True, allow_unsafe_werkzeug=True, ssl_context='adhoc')
    
    # standard
     socketio.run(app, host='10.0.0.140', port=5000, debug=True, allow_unsafe_werkzeug=True)

# 10.0.0.140 (rpi IP)
