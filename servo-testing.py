from gpiozero import Servo
from gpiozero.pins.pigpio import PiGPIOFactory
import math
from time import sleep

factory = PiGPIOFactory()

servo = Servo(23, min_pulse_width=0.5/1000, max_pulse_width=2.5/1000, pin_factory=factory)

def radar():
    while True:
    for i in range(0, 360):
        servo.value = math.sin(math.radians(i))
        sleep(0.01)

def move_degree(degrees):
    sleep(0.01)
    servo.value = math.sin(math.radians(int(degrees)))
