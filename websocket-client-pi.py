import socketio

from flask import Flask
from flask_cors import CORS # sec by pass
from flask_socketio import SocketIO
import time

from gpiozero import Servo
from gpiozero.pins.pigpio import PiGPIOFactory
import math

import base64
import io
from picamera import PiCamera


factory = PiGPIOFactory()
servo1 = Servo(23, min_pulse_width=0.5/1000, max_pulse_width=2.5/1000, pin_factory=factory)
servo2 = Servo(24, min_pulse_width=0.5/1000, max_pulse_width=2.5/1000, pin_factory=factory)

app = Flask(__name__)

@app.route('/') 
def index():
    print('online')

sio = socketio.Client()

sio.connect("https://se101-backend-production.up.railway.app/")

sio.emit("join", 1)
    
@sio.event
def move(data):
    angle = data["distance"]
    direction = data["direction"]
    
    if (direction == "left"):
        angle = -angle
    if (direction == "down"):
        angle = -angle
    angle = math.sin(math.radians(int(angle)))
    print(f'The angle is {angle}')
    
    if (direction == "left" or direction == "right"):  
        if (servo1.value +  angle < 1 and servo1.value +angle > -1):
            servo1.value += angle
    else:
        if (servo2.value +  angle < 1 and servo2.value +angle > -1):
            servo2.value += angle
    
def capture_image():
    camera = PiCamera(resolution=(640,480))
    camera.brightness=65 # above 50 = increase
    camera.contrast = 40 # positive = increase
    camera.vflip=True
    image_stream = io.BytesIO()
    
    camera.start_preview()
    camera.capture(image_stream, format='jpeg')
    
    time.sleep(0.1)
    camera.stop_preview()
    camera.close()
     
    image_stream.seek(0)
    
    image_data = base64.b64encode(image_stream.read()).decode('utf-8')
    
    print('Got image data')
    
    return image_data

while True:
    time.sleep(10)
    sio.emit("image", capture_image())

print(12)
